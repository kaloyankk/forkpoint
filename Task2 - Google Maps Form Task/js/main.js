window.onload = function(){
     var cord = {};
     var exampleLat;
     var exampleLng;
     var clickedAddress;
     var map;
     var uluru;
     let name,
            email,
            address,
            phone,
            website;
     function initMap(parsedLat, parsedLng) {

        uluru = {lat: parsedLat , lng: parsedLng};
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 18,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      } 
    document.getElementById("submit-button").onclick = function(){
    //Declaring every input field into variable
        
        name = document.getElementById("name").value;
        email = document.getElementById("email").value;
        address = document.getElementById("address").value;
        phone = document.getElementById("phone").value;
        website = document.getElementById("website").value;


        var objFromForm = {
            "name" : name,
            "email" : email,
            "address" : address,
            "phone" : phone,
            "website" : website
        };
    
        localStorage.setItem("Current Object", JSON.stringify(objFromForm));

        var receivedObj = localStorage.getItem("Current Object");
        var parsedObj = JSON.parse(receivedObj);
        var addressFromObj = parsedObj.address;
        

        var geocoder = new google.maps.Geocoder();
    geocoder.geocode( { 'address' : addressFromObj }, function(result){
        var latitude = result[0].geometry.location.lat();
        var longitude = result[0].geometry.location.lng();

        localStorage.setItem("lat", JSON.stringify(latitude));
        localStorage.setItem("lng", JSON.stringify(longitude));
    })

    var receivedLat = localStorage.getItem("lat");
    var receivedLng = localStorage.getItem("lng");
    var parsedLat = JSON.parse(receivedLat);
    var parsedLng = JSON.parse(receivedLng);  

    initMap(parsedLat,parsedLng);
  }
  document.getElementById("find-manually").onclick = function(){
      exampleLat = 42.697708;
      exampleLng = 23.321868;
      initMap(exampleLat,exampleLng)

      google.maps.event.addListener(map, 'click', function (event) {
        displayCoordinates(event.latLng);               
    });

      function displayCoordinates(pnt) {

        var lat = pnt.lat();
        lat = lat.toFixed(4);
        var lng = pnt.lng();
        lng = lng.toFixed(4);
        var newLatLng = new google.maps.LatLng(lat, lng);
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode( { 'latLng' : newLatLng
            }, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {  
                if(results[0]){
                    clickedAddress = results[0].formatted_address;
                    document.getElementById("address").focus();
                    document.getElementById('address').value = clickedAddress;

                }
            }
        }); 
    }   
  }
}
